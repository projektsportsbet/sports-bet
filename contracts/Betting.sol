pragma solidity ^0.4.20;

contract Betting{
    address public owner;
    uint256 public minBet;
    uint256 public totalBet1;
    uint256 public totalBet2;
    uint256 public odds1;
    uint256 public odds2;
    uint256 public noOfBets;
    uint256 public maxBets = 1000;
    
    address[] public players; //An array of addresses which will contain all the players addresses
    
    struct Player{
        uint256 amountBet; //Object for the player information
        uint16 teamSelected;
    }
    
    mapping(address => Player) public playerInfo; //To link the player address to some players infos
    
    function() public payable {}
    function Betting() public {
      owner = msg.sender;
      minBet = 100000000000000;
    }
    function kill() public {
      if(msg.sender == owner) selfdestruct(owner);
    }
    
    function checkPlayerExists(address player) public constant returns(bool){
        for(uint256 i=0;i<players.length;i++){
            if(players[i]==player) return true; //Check if the player has already placed a bet or not
        }
        return false;
    }
    
    function bet(uint8 _teamSelected) public payable{
        require(!checkPlayerExists(msg.sender)); // Check if player already checkPlayerExists
        require(msg.value>=minBet); // Check if bet is of valid amount
        
        playerInfo[msg.sender].amountBet = msg.value; // Setting player values
        playerInfo[msg.sender].teamSelected = _teamSelected;
        
        players.push(msg.sender); //then we add the address of the player to the players array
        
        if(_teamSelected == 1){          // Increment stakes on team if all check passes
            totalBet1 += msg.value;
        }
        else{
            totalBet2 += msg.value;
        }

        odds1 = ((totalBet1*100)/(totalBet1+totalBet2));
        odds2 = ((totalBet2*100)/(totalBet1+totalBet2));

    }
    
    function distributePrizes(uint16 teamWinner) public{
      address[1000] memory winners; 
      //We have to create a temporary array with fixed size
      //Let's choose 1000
      uint256 count = 0; // This is the count for the array of winners
      uint256 LoserBet = 0; //This will take the value of all losers bet
      uint256 WinnerBet = 0; //This will take the value of all winners bet
      
      //We loop through the player array to check who selected the winner team
      for(uint256 i = 0; i < players.length; i++){
         address playerAddress = players[i];
         
         //If the player selected the winner team
         //We add his address to the winners array
         if(playerInfo[playerAddress].teamSelected == teamWinner){
            winners[count] = playerAddress;
            count++;
         }
      }
      
      // Assign total losing and winning team bets
      if ( teamWinner == 1){
         LoserBet = totalBet2;
         WinnerBet = totalBet1;
      }
      else{
          LoserBet = totalBet1;
          WinnerBet = totalBet2;
      }
      
      //Looping through the array of winners, to give winnings to the winners
      for(uint256 j = 0; j < count; j++){
         if(winners[j] != address(0)) // Check that the address in this fixed array is not empty
            address add = winners[j];
            uint256 bet = playerInfo[add].amountBet;
            //Transfer the money to the user 
            winners[j].transfer(bet+((bet/WinnerBet)*LoserBet));
      }
      delete playerInfo[playerAddress]; // Delete all the players
      players.length = 0; // Delete all the players array
      LoserBet = 0;
      WinnerBet = 0;
      totalBet1 = 0;
      totalBet2 = 0;
      odds1 = 0;
      odds2 = 0;
   }
   
   function AmountOne() public view returns(uint256){
       return totalBet1; // Getters used by the frontend
   }
   
   function AmountTwo() public view returns(uint256){
       return totalBet2; //Getters used by the frontend
   }

   function PerOne() public view returns(uint256){
        //Getters used by the frontend
       return odds1;
   }

   function PerTwo() public view returns(uint256){
       //Getters used by the frontend
       return odds2;
   }
}
import React, { Component } from "react";
import getWeb3 from "./utils/getWeb3.js";
import BettingContract from "./contracts/Betting.json";
import "./App.css";

class TeamA extends Component {
  constructor() {
    super();
    this.state = {
      web3: "",
      Amount: "",
      winP: "",
      multi: "",
      InputAmount: "",
      weiConversion: 1000000000000000000
    };

    this.getAmount = this.getAmount.bind(this);
    this.Bet = this.Bet.bind(this);
    this.MakeWin = this.MakeWin.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.getOdds = this.getOdds.bind(this);
  }

  componentDidMount() {
    getWeb3
      .then(results => {
        /*After getting web3, we save the informations of the web3 user by
          editing the state variables of the component */
        results.web3.eth.getAccounts((error, acc) => {
          //this.setState is used to edit the state variables
          this.setState({
            web3: results.web3
          });
        });
        //At the end of the first promise, we return the loaded web3
        return results.web3;
      })
      .then(results => {
        //In the next promise, we pass web3 (in results) to the getAmount function
        this.getAmount(results);
      })
      .catch(() => {
        //If no web3 provider was found, log it in the console
        console.log("Error finding web3.");
      });
  }

  getAmount(web3) {
    //Get the contract
    const contract = require("truffle-contract");
    const Betting = contract(BettingContract);
    Betting.setProvider(web3.currentProvider);
    var BettingInstance;
    web3.eth.getAccounts((error, accounts) => {
      Betting.deployed()
        .then(instance => {
          //Instanciate the contract in a promise
          BettingInstance = instance;
        })
        .then(result => {
          //Calling the AmountOne function of the smart-contract
          this.getOdds();
          return BettingInstance.AmountOne.call({ from: accounts[0] });
        })
        .then(result => {
          //Then the value returned is stored in the Amount state var.
          //Divided by 10000 to convert in ether.
          this.setState({
            Amount: result.c / 10000
          });
        });
    });
  }

  Bet() {
    const contract = require("truffle-contract");
    const Betting = contract(BettingContract);
    Betting.setProvider(this.state.web3.currentProvider);
    var BettingInstance;
    this.state.web3.eth.getAccounts((error, accounts) => {
      Betting.deployed()
        .then(instance => {
          BettingInstance = instance;
        })
        .then(result => {
          // Get the value from the contract to prove it worked.
          return BettingInstance.bet(1, {
            from: accounts[0],
            value: this.state.InputAmount
          });
        })
        .catch(() => {
          console.log("Error with betting");
        });
    });
  }

  MakeWin() {
    const contract = require("truffle-contract");
    const Betting = contract(BettingContract);
    Betting.setProvider(this.state.web3.currentProvider);
    var BettingInstance;
    this.state.web3.eth.getAccounts((error, accounts) => {
      Betting.deployed()
        .then(instance => {
          BettingInstance = instance;
        })
        .then(result => {
          return BettingInstance.distributePrizes(1, { from: accounts[0] });
        })
        .catch(() => {
          console.log("Error with distributing prizes");
        });
    });
  }

  getOdds() {
    //Get the contract
    const contract = require("truffle-contract");
    const Betting = contract(BettingContract);
    Betting.setProvider(this.state.web3.currentProvider);
    var BettingInstance;
    this.state.web3.eth.getAccounts((error, accounts) => {
      Betting.deployed()
        .then(instance => {
          //Instanciate the contract in a promise
          BettingInstance = instance;
        })
        .then(result => {
          return BettingInstance.PerOne.call({ from: accounts[0] });
        })
        .then(result => {
          //Then the value returned is stored in the Amount state var.
          //Divided by 10000 to convert in ether.
          this.setState({
            winP : result.c,
            multi: (100 / result.c).toFixed(2)
          });
          if(this.state.winP == "0"){
            this.setState({
              multi: 0.0
            });
          }
        });
    });
  }

  handleInputChange(e) {
    this.setState({ InputAmount: e.target.value * this.state.weiConversion });
  }

  render() {
    return (
      <div>
        <img
          src="http://www.stickpng.com/assets/images/580b585b2edbce24c47b2804.png"
          className="india"
          alt="india_flag"
        />
        <h4 className="total-amount">
          {" "}
          Total amount : <span className="amount">{this.state.Amount}</span> ETH
        </h4>
        <h5>
          {" "}
          Win Percentage : <span className="amount">{this.state.winP}</span>%
        </h5>
        <h5>
          {" "}
          Multiplier : <span className="amount">{this.state.multi}</span>x
        </h5>
        <hr />
        <h5> Enter an amount to bet</h5>
        <div className="input-group">
          <input
            type="text"
            className="form-control"
            onChange={this.handleInputChange}
            required
            pattern="[0-9]*[.,][0-9]*"
          />
          <span className="input-group-addon">ETH</span>
        </div>
        <br />
        <div onClick={this.Bet} className="bet-button">
          Bet
        </div>
        <br />
        <hr />
        <div onClick={this.MakeWin} className="win-button">
          Make this team win
        </div>
      </div>
    );
  }
}

export default TeamA;
